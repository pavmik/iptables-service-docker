#!/bin/bash
# Install

while true; do
    read -p "Do you wish to install this program? (y/n)" yn
    case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
    esac
done


DATE=`date +%Y-%m-%d`

DEF_INT=`ip rou | awk '/default/ { print $5 }'`


#backup old
mv /etc/sysconfig/iptables  /etc/sysconfig/iptables.$DATE.bak
mv /etc/systemd/system/iptables.service  /etc/systemd/system/iptables.service.$DATE.bak

#install new
cp iptables /etc/sysconfig/iptables
cp iptables.service /etc/systemd/system/iptables.service
systemctl daemon-reload
systemctl enable iptables > /dev/null

#fix default interface name
echo Fixing default interface to $DEF_INT
sed -i -e ''s/eth0/$DEF_INT/g'' /etc/sysconfig/iptables

#info
echo
echo "Check your config in /etc/sysconfig/iptables and start new firewall with:"
echo systemctl start iptables

