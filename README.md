# iptables-docker

Iptables config for Docker host (Centos 7, RHEL,...)


### Install
Replace standard iptables-service
`./install.sh`

## Updating the firewall
Edit /etc/sysconfig/iptables and run
`$ sudo systemctl restart iptables`